# qt5_ex.py

import sys
from PyQt5.QtWidgets import (
        QApplication, QWidget, QLabel, QPushButton, QComboBox, QFrame, QMainWindow,
        QMessageBox, QLineEdit, QTextEdit

    )
from PyQt5.QtCore import pyqtSlot, QRect, Qt
from PyQt5.QtGui import QTextCursor

import MySQLdb as mq

class MainPage(QMainWindow):
    def __init__(self, title=" "):
        super().__init__()  # inherit init of QWidget
        self.title = title
        self.left = 250
        self.top = 50
        self.width = 600
        self.height = 250
        self.widget()

    def widget(self):
        # window setup
        self.setWindowTitle(self.title)
        # self.setGeometry(self.left, self.top, self.width, self.height)
        ## use above line or below
        # self.resize(self.width, self.height) # resizable
        self.setFixedSize(self.width, self.height)  # fixed size
        self.move(self.left, self.top)

        # create frame for a set of checkbox
        self.frame1 = QFrame(self)
        self.frame1.setGeometry(QRect(40, 40, 250, 250))

        # create table
        self.tbl_name = QLineEdit(self.frame1)
        self.tbl_name.setPlaceholderText("Table name")
        self.tbl_name.setGeometry(0, 0, 100, 30)

        self.btn1 = QPushButton(self.frame1, text="Create Table")
        self.btn1.move(0, 50)
        self.btn1.clicked.connect(self.create_table)

        # insert data
        self.tbl_name2 = QLineEdit(self.frame1)
        self.tbl_name2.setPlaceholderText("Table name")
        self.tbl_name2.setGeometry(150, 0, 100, 30)

        self.name = QLineEdit(self.frame1)
        self.name.setPlaceholderText("Name")
        self.name.setGeometry(150, 50, 100, 30)

        self.age = QLineEdit(self.frame1)
        self.age.setPlaceholderText("Age")
        self.age.setGeometry(150, 100, 100, 30)

        self.btn2 = QPushButton(self.frame1, text="Insert")
        self.btn2.move(150, 150)
        self.btn2.clicked.connect(self.insert_data)

        
        self.frame2 = QFrame(self)
        self.frame2.setGeometry(QRect(350, 40, 250, 250))
        # show data 
        self.txtbox1 = QTextEdit(self.frame2)
        self.txtbox1.setGeometry(0, 0, 200, 140)

        self.tbl_name3 = QLineEdit(self.frame2)
        self.tbl_name3.setText("writer")
        self.tbl_name3.setGeometry(0, 150, 100, 25)

        self.btn4 = QPushButton(self.frame2, text="Show data")
        self.btn4.move(110, 150)
        self.btn4.clicked.connect(self.show_data)
        self.show()

    def connect_db(self):
        try:
            self.conn = mq.connect(host='localhost', user='root', password='root', db='qtdb')
            self.cursor = self.conn.cursor()
            print("Connected")
        except mq.Error as err:
            print(err)

    def disconnect_db(self):
        """ commit changes to database and close connection """
        self.conn.commit()
        self.cursor.close()
        self.conn.close()
        print("Disconnected")

    def insert_data(self):
        self.connect_db()
        self.cursor.execute("INSERT INTO %s (name, age) VALUES ('%s',%s)" % (
            self.tbl_name2.text(), 
            self.name.text(), 
            self.age.text()
            )
        )
        self.disconnect_db()
        QMessageBox.about(self, "Insert", "Data inserted successfully")

    def create_table(self):
        """ Create table in the database """

        self.connect_db()
        # optional: drop table if exists
        self.cursor.execute('DROP TABLE IF EXISTS %s' % self.tbl_name.text())
        self.cursor.execute('CREATE TABLE %s \
                (                   \
                  id    INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, \
                  name  VARCHAR(30) NOT NULL,  \
                  age   int \
                )' % self.tbl_name.text()
        )
        print("Table created")
        QMessageBox.about(self, "Create", "Table created successfully")
        self.disconnect_db()
        
    def show_data(self):
        self.connect_db()
        self.cursor.execute("SELECT * FROM %s" % self.tbl_name3.text())
        data = self.cursor.fetchall()
        self.disconnect_db()

        self.txtcursor = QTextCursor(self.txtbox1.document())
        text = "{0:<2s} {1:<10s} {2:<3s}".format("Id", "Name", "Age")
        self.txtcursor.insertText(text + "\n")
        for d in data:
            text = "{0:<2d} {1:<10s} {2:<3d}".format(d[0], d[1], d[2])
            self.txtcursor.insertText(text + "\n")
            print(text)
        

def main():
    app = QApplication(sys.argv)
    w = MainPage(title="PyQt5")
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
