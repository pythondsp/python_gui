# tkoops.py

import tkinter as tk

# inherit from tk.Frame
class WindowSetup(tk.Tk):
    def __init__(self, title):
        super().__init__()  # inherit parent class init
        # set size of window
        self.geometry("500x400+10+10") # width, height, left margin, top margin

        # frame
        self.frame = tk.Frame(self) # create frame
        self.frame.master.title(title) # title of frame
        self.frame.grid(padx=20, pady=20) # add some margin from top and bottom


class HomePage(WindowSetup):
    def __init__(self, title=" "):
        super().__init__(title)
        self.createWidgets() # function call : createWidgets

    def createWidgets(self):
        # create Label
        self.myLabel = tk.Label(self.frame, text="This is Home page")
        # set grid parameters
        self.myLabel.grid(row=0, column=0)

        #create button
        self.btn1 = tk.Button(self, text="License", command=self.licensePage)
        self.btn1.grid(row=1, column=0)

    def licensePage(self):
        self.frame = tk.Frame(self) # create frame
        app = License("License Page")


class License(WindowSetup):
    def __init__(self, title=" "):
        super().__init__(title)
        self.createWidgets() # function call : createWidgets

    def createWidgets(self):
        # create Label
        self.myLabel = tk.Label(self.frame, text="This is License Page")
        # set grid parameters
        self.myLabel.grid(row=0, column=0)

        #create button
        self.btn1 = tk.Button(self, text="Home Page", command=self.homepage)
        self.btn1.grid(row=1, column=0)

    def homepage(self):
        app = HomePage("Home Page")

def main():
    app = HomePage("Home Page")
    app.mainloop()

if __name__ == '__main__':
    main()
