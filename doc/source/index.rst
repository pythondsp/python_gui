.. Python GUI documentation master file, created by
   sphinx-quickstart on Fri May 11 18:44:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Python GUI (Under progress)
===========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   gui/qt
   gui/tkinter